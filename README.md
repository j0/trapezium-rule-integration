
Fun little program that approximates the area under the curve. (Integration!) It does this by drawing trapezia under a curve, then adds more over time to get a better approximation of the real area.

This program is not very capable, and while it eventually creates a good approximation, it will take a while. This was written for fun and should not be used for anything serious! :)


![Inspired by 3blue1brown's Essense of Calculus series](https://www.youtube.com/watch?v=WUvTyaaNkzM#cloudtube)