use std::time::{Duration, Instant};

fn main() {
    println!("Hello, integration!");
    let min = 0.0;
    let max = 3.0;
    let accuracy = 2.0_f64.powf(30.0); // This is how many trapezia it will "draw"

    boring_display_stuff(min, max, accuracy)

    // This is equivalent to:

    //  3
    //  /`
    //  |  (5 / x^2) + 1 dx
    // ,/
    //  0

}

fn f(x: f64) -> f64 {
    5.0 / (x.powf(2.0) + 1.0)
}



fn find_steps(min: f64, max: f64, h: f64) -> Vec<f64> {

    let mut i = min;
    let mut steps = vec![];

    loop {
        if i >= max {
            break;
        }
        steps.push(f(i));
        i += h;
    }
    steps
}

fn area(min: f64, max: f64, h: f64) -> (f64, Duration) {

    let start = Instant::now();

    let steps = find_steps(
        min,
        max,
        h,
    );

    let mid : f64 = steps.into_iter().sum();

    let a = f(min) + f(max);
    let b = mid;

    (h * 0.5 * ( a + (2.0 * b) ) , start.elapsed() )
}

fn boring_display_stuff(min: f64, max: f64, inverse_h: f64) {
    let start = Instant::now();


    let mut approx: f64;
    let mut h = 1.0;
    loop {
        if h < (1.0 / inverse_h) {break;}

        let (a,t) = area(min, max, h);
        approx = a;
        let measure: &str;
        let mut t = t.as_millis();
        if t < 1000 {
            measure = "ms";
        } else {
            t = Duration::from_millis(t as u64).as_secs() as u128;
            measure = "s";
        }
        println!("{} trapezia, area is about {approx} (taken {t}{measure})", (1.0/h) as usize );
        h /= 2.0;
    }
    println!("You reached the base case! Congrats!");
    println!("Taken {}s", start.elapsed().as_secs())
}